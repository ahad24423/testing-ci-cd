const request = require("supertest");
const server = require("./index"); // Import the server

// Test the '/' route
describe("GET /", () => {
  it('responds with "Hello, World!"', async () => {
    const response = await request(server).get("/"); // Use server instead of app
    expect(response.status).toBe(200);
    expect(response.text).toBe("Hello world from the gitlab ci cd");
  });
});
