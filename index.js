const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("Hello world from the gitlab ci cd");
});

const server = app.listen(5000, () => {
  console.log("Server is running on port 5000"); // Move this log statement
});

module.exports = server; // Export the server instead of the app
